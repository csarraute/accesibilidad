%!TEX root = tech-report.tex

%--------------------------------------------------------------------------
\section{Introducción}
%--------------------------------------------------------------------------


%--------------------------------------------------------------------------
\subsection{Problemática analizada y contexto en el que se inserta}
%--------------------------------------------------------------------------

	El crecimiento espacial de una población, la caracterización sociodemográfica de una región, la determinación de factores de exclusión social y la evaluación de la infraestructura de las redes de transporte parecen temas cuyo factor común es el directo impacto en la sociedad. Sin embargo, estas problemáticas son atravesadas transversalmente por la accesibilidad geográfica, es decir, la posibilidad de que todas las personas transiten fácilmente, mediante el uso del entorno construido, hacia destinos de interés propios. En la última década se ha puesto principal foco en generar medidas capaces de determinar la calidad de la accesibilidad a destinos tales como centros de salud, trabajo y escuelas, los cuales juegan un rol principal en los temas presentados.

A la hora de estimar esta accesibilidad, es frecuente el uso de servicios de mapas online tales como GoogleMaps u OpenStreetMap. Sin embargo, estos servidores de aplicaciones cuentan con múltiples desventajas: funcionan como cajas cerradas (no se puede modificar su contenido, como agregar calles o direcciones), poseen limitaciones en la búsqueda de gran cantidad de datos y dependen de una conexión a internet para realizar cualquier tipo de consulta. Además, poseen políticas de retención de información, las cuales –en muchas ocasiones- pueden entrar en conflicto con la confidencialidad del usuario y la seguridad de los datos de la búsqueda.

Se propone luego la creación de la primera herramienta abierta capaz de brindar información acerca de la accesibilidad geográfica de la Argentina, completamente transparente, gratuita, offline y adaptable a las necesidades del usuario. La misma contará con las redes de transporte público y ubicaciones a oportunidades de interés. Asimismo, operará con ubicaciones puntuales (es decir, tanto el origen como el destino se encontrarán determinados por coordenadas geográficas), por lo que funcionará como la estructura atómica a la hora de construir indicadores de escalas más grandes, tales como municipios o provincias. El usuario podrá especificar el modo con el que la herramienta calculará la accesibilidad a las ubicaciones: a pie, en bicicleta, en auto o en transporte público. El programa arrojará un conjunto de medidas (distancia y tiempo) las cuales servirán como materia prima para la confección de posteriores métricas de accesibilidad. Algunas aplicaciones directas de esta herramienta se enumeran a continuación:

\begin{itemize}
\item La generación de un conjunto de índices capaces de reflejar el grado de accesibilidad de una vivienda a sus oportunidades más relevantes para los integrantes de el/los hogares que lo componen.
\item La capacidad de comparar cuantitativamente la accesibilidad de regiones geográficas a ofertas de trabajo mediante el uso de curvas que delimiten áreas de movilidad, invirtiendo la misma cantidad de tiempo de viaje dentro de ellas (isócronas).
\item La formulación de indicadores de saturación a oportunidades tales como escuelas u hospitales, basándose en la cantidad de hogares con potencial a utilizarlos, en una determinada cercanía.

\end{itemize}


En todos estos ejemplos, no sólo es necesaria una herramienta capaz de medir accesibilidad urbana, si no también poseer información detallada tanto de los puntos de partida (hogares) como los de llegada (oportunidades). En este aspecto, el Ministerio de Producción y Trabajo (MPyT) posee una destacada trayectoria en el campo, manejando registros de encuestas y censos, los cuales permiten caracterizar a los hogares y conocer sus potenciales necesidades frente a las oportunidades. Asimismo, en los últimos años se han generado bases de datos de escuelas y hospitales a nivel nacional, las cuales permiten una identificación georreferenciada y descriptiva de estos establecimientos.

	Además de las nuevas líneas de investigación que permitirá abrir esta herramienta, es posible enumerar ámbitos ya existentes en donde su aplicación significará una cuantitativa mejora o un enfoque desde otro marco de referencia.
	
\begin{itemize}
\item Eficiencia en las fiscalizaciones/GECAL: La determinación de distancias óptimas tendrá un inmediato impacto para las personas que trabajan en estas áreas.
\item Análisis de oferta y demanda laboral: Es posible establecer el vínculo entre viviendas y las empresas donde trabajan los integrantes de los hogares a nivel geográfico. Luego, se puede estudiar cómo afecta la accesibilidad a la hora de reducir la distancia entre estas dos ubicaciones.
\item Transporte al trabajo: La herramienta permitirá realizar aportes a un diseño de un sistema de transporte que reduzca el tiempo/dinero de viaje de los usuarios.
\item Mapas de crecimiento de empleos y empresas: Dada la resolución atómica de la herramienta, es posible luego realizar un estudio a nivel regional y estudiar la dinámica de crecimiento de empleos de una determinada zona teniendo en cuenta su accesibilidad media.
\item Estudio de migraciones: Mediante un cruce de datos de migración y constancias laborales, se podrá cuantificar el factor de accesibilidad a la hora de tomar la decisión de mudarse de una vivienda a otra, por cercanía al trabajo.

\end{itemize}
	

Por último, cabe destacar que la herramienta a desarrollar no se encontrará solamente limitada al uso del Ministerio de Producción y Trabajo, sino que quedará a disposición para su uso en otras dependencias públicas. 

	Algunos ejemplos de servicios y oportunidades para los cuales estamos interesados en evaluar la accesibilidad incluyen:
\begin{itemize}
\item Fuentes de empleo 
\item Establecimientos educativos
\item Centros de salud, farmacias
\item Sucursales bancarias, cajeros
\item Fuerzas de seguridad
\end{itemize}


La métrica de accesibilidad que proponemos desarrollar es la cercanía a las oportunidades urbanas. Específicamente, proponemos estimar distancias y tiempos que los habitantes deben invertir en viajar hasta las oportunidades estudiadas, en función de distintos medios de transporte.

Proponemos evaluar varias alternativas de medios de transporte:
\begin{itemize}
\item a pie, 
\item en bicicleta, 
\item en auto, 
\item en transporte público (trenes, subtes, colectivos).
\end{itemize}



%--------------------------------------------------------------------------
\subsection{Producto de ciencia de datos propuesto}
%--------------------------------------------------------------------------

El objetivo del proyecto es desarrollar una herramienta para calcular distancias y tiempos de viaje entre:
\begin{itemize}
\item un conjunto de orígenes,
\item un conjunto de destinos, 
\item usando distintas modalidades de transporte.
\end{itemize}


Los datos de entrada (inputs) para la herramienta serán:
\begin{itemize}
\item Destinos: Un listado georreferenciado de puntos de interés u oportunidades (tales como establecimientos de empleadores, escuelas, farmacias, fuerzas seguridad, etc.)
Por ejemplo: un listado con 61.000 colegios en Argentina.

\item Orígenes: La herramienta contempla varias formas de especificar los orígenes:
\begin{itemize}
\item Listado de puntos (coordenadas geográficas).
\item Por ejemplo: coordenadas de viviendas encuestadas en la ENAPROSS 2015.
\item Todas las esquinas del AMBA.
\item Centroides de radios o fracciones censales.
\item Centroides de municipios.
\end{itemize}

\end{itemize}

En base a los datos de entrada, la herramienta se ocupará de determinar, desde cada origen, cuál es el destino más cercano, en función de la distancia o del tiempo de viaje y de la modalidad de transporte. Las opciones que contemplamos son:
\begin{itemize}
\item Mínima distancia: 
\begin{itemize}
\item A pie
\item En auto

\end{itemize}


\item Tiempo mínimo de viaje:
\begin{itemize}
\item A pie
\item En bicicleta
\item En auto
\item En transporte público

\end{itemize}


\end{itemize}


Cabe destacar que los conjuntos de orígenes y destinos son conjuntos grandes, que suponemos tienen varios miles de puntos (tanto orígenes como destinos), por lo cual calcular el camino mínimo desde todos los orígenes a todos los destinos es computacionalmente muy ineficiente. Desarrollaremos una implementación eficiente para calcular caminos mínimos en este contexto.

Nuestro segundo objetivo con este desarrollo es usar exclusivamente datos abiertos, esto es, datos disponibles libremente y de acceso gratuito, tales como los datos de Open Street Map. Consideramos un factor clave el hecho de usar datos abiertos y código open source, para que el uso de esta herramienta no dependa de factores externos tales como: los datos de una compañía privada, el acceso arancelado a una API de una compañía privada.


%--------------------------------------------------------------------------
\subsection{Estado de avance con el que se proyecta entregar el producto final (entregables)}
%--------------------------------------------------------------------------

Planeamos entregar una herramienta que pueda ser utilizada inmediatamente. Los pasos siguientes para efectivizar una implementación exitosa de la herramienta desarrollada en la institución pública (Secretaría de Promoción, Protección y Cambio Tecnológico del Ministerio de Producción y Trabajo, MPyT) son:
\begin{itemize}
\item Capacitar a los equipos técnicos del MPyT en el funcionamiento del código. Este no deberá ser una caja negra sino, por el contrario, una herramienta capaz de ser mejorable a futuro.
\item Explicar a los usuarios de la herramienta (funcionarios con conocimientos de programación y herramientas para el análisis geográfico de datos) en cómo utilizarla con futuros datos.
\item Trabajar en el análisis de los datos generados por la herramienta, y en su visualización efectiva.

\end{itemize}


Además de la herramienta para calcular las métricas de accesibilidad propuestas, planeamos validar el desarrollo mediante la utilización de la misma en los siguientes casos:
\begin{itemize}
\item Generar mapas de calor (heatmaps) para casos de uso específicos, como la accesibilidad a escuelas en AMBA. Se entregarán tanto los mapas como los datos crudos para generarlos (resultados de la aplicación de la herramienta).
\item Comparar accesibilidad a escuelas y centros de salud con los resultados de ENAPROSS 2015.
\item Comparar con los análisis de movilidad en CABA realizados para CAF por Sebastián Anapolsky.

\end{itemize}


%--------------------------------------------------------------------------
\subsection{Productos de ciencia de datos previos relacionados}
%--------------------------------------------------------------------------

Los científicos de datos que integran el equipo han trabajado previamente en estudios sobre movilidad urbana en la ciudad de Buenos Aires. Esta experiencia les permitió desarrollar conocimientos sobre datos abiertos relevantes para el proyecto, tales como los datos de Open Street Map. También desarrollaron conocimientos sobre los algoritmos que permiten analizar datos de movilidad y estimar caminos mínimos (tanto en tiempo como en distancia).

Parte de estos desarrollos previos están descritos en los siguientes trabajos:
\begin{itemize}
\item Exploración y análisis de datos de telefonía celular para estudiar comportamientos de movilidad en la ciudad de Buenos Aires~\cite{Anapolsky2014exploracion}
\item The city pulse of Buenos Aires~\cite{Sarraute2017city}
\end{itemize}


También han desarrollado técnicas de análisis de movilidad en los siguientes trabajos:
\begin{itemize}
\item Human mobility and predictability enriched by social phenomena information~\cite{Ponieman2013human}
\item Mobility and sociocultural events in mobile phone data records~\cite{Ponieman2015mobility}
\item On the regularity of human mobility~\cite{Mucelli2016regularity}
\item Measurement-driven mobile data traffic modeling in a large metropolitan area~\cite{Oliveira2015measurement}
\end{itemize}


