import sys, os
import csv
from random import randrange
import json

# Este script pasa los datos de recorridos de colectivos de CSV a JSON
# de forma que podamos operar con mas facilidad
# El JSON queda con el formato explicado en el tech-report

# Dado un LINESTRING de WKT, devuelve la lista de paradas
# Cada parada tiene un campo "coords"
def procesarParadas(s):
    startOfData = 12    # Largo del string "LINESTRING ("
    endOfData = -1      # -Largo del string ")"
    
    paradas = s.strip()[startOfData:endOfData].split(",")
    res = []
    for p in paradas:
    coords = [float(x) for x in p.strip().split(" ")]
    if len(coords) == 2:
        res.append({"coords": coords})
    
    return res

# Pasa el csv de recorridos de colectivos a un JSON
# para poder utilizarlo en otros scripts
def cargarLineasColectivos():
    path = "../data/transporte-publico/recorrido-colectivos-utf8.csv"

    lineas = []
    with open(path) as f:
        csvReader = csv.reader(f, delimiter = ';')
        next(csvReader) # Salteo el header
        for row in csvReader:
            elem = {}
            elem["id"] = int(row[1])
            elem["linea"] = row[2]
            elem["tipo"] = row[3]
            elem["ramal"] = row[4]
            elem["sentido"] = row[5]
            elem["paradas"] = procesarParadas(row[0])
            lineas.append(elem)
    return lineas

def main(argc, argv):
    colectivos = cargarLineasColectivos()
    outFilePath = "../data/transporte-publico/"+argv[1]+".json"
    with(open(outFilePath, 'w')) as f:
        json.dump(colectivos, f)

if __name__ == '__main__':
    main(len(sys.argv), sys.argv)
