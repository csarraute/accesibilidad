import sys, os
import json
import time
from util import distInMeters

# Este script agrega los horarios a los colectivos
# Requiere haber compilado el script osrm-route
# Hay varios comentarios utiles para debug

MAPA = "../data/mapa-auto/buenos_aires_rectangle.osrm"
COLECTIVOS = "../data/transporte-publico/recorrido-colectivos.json"
OUT = "../data/transporte-publico/recorrido-colectivos-horarios2.json"

def cargarColectivos():
	colectivosGCBA = []
	with(open(COLECTIVOS)) as f:
		colectivosGCBA = json.load(f)

	return colectivosGCBA

# Dada una linea con su lista de paradas,
# le pedimos a OSRM que calcule el tiempo entre una y la siguiente
# utilizando el servicio Route
def calcularTiempo(linea, paradas):
	# Construimos el comando para llamar a osrm-route
	tokens = ["./build/osrm-route", MAPA]
	tempFilename = "../data/temp_tiempo_paradas.txt"

	# Agregamos la lista de paradas al comando
	for p in paradas:
		for x in p["coords"]:
			tokens.append(str(x))

	tokens.append(">")
	tokens.append(tempFilename)
	# print((" ").join(tokens))
	# Corremos el script
	os.system((" ").join(tokens))

	with(open(tempFilename)) as f:
		tiempoAcumulado = 0 # Contador de tiempo transcurrido desde la terminal
		paradas[0]["horario"] = tiempoAcumulado
		res = f.read()
		# Vemos si hubo un error
		if res.strip() == "NoRoute":
			print("La linea ", linea, " no pudo rutearse!")
		else:
			ts = res.split()
			for i in range(1, len(paradas)):
				try:
					# Vamos cargando los tiempos
					tiempoAcumulado += float(ts[i-1])
				except:
					print("Fallamos en: ", linea, "     \"", res, "\"")
					print((" ").join(tokens))
				paradas[i]["horario"] = tiempoAcumulado


def agregarTiempos(colectivos):
	# progreso = 0
	for c in colectivos: # for ci in range(5):
		# progreso += 1
		# print(progreso, "/", len(colectivos))
		# print(" - ".join([c["linea"],c["tipo"],c["ramal"],c["sentido"]]))
		# tiempoEjecucion = time.time()
		calcularTiempo(" - ".join([c["linea"],c["tipo"],c["ramal"],c["sentido"]]), c["paradas"])
		# print([p["horario"] for p in c["paradas"]])
		# print(time.time() - tiempoEjecucion)


def main(argc, argv):
	colectivos = cargarColectivos()
	agregarTiempos(colectivos)

	with(open(OUT, 'w')) as f:
		json.dump(colectivos, f)


if __name__ == '__main__':
	main(len(sys.argv), sys.argv)
