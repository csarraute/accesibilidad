#include <iostream>
#include <cstdio>
#include <vector>
#include <cmath>
#include <string>

using namespace std;

struct punto{
	double x,y;
	int id;
	punto(){
		x=y=0.;
		id=-1;
	}
	punto(int idd, double xx, double yy){
		id = idd;
		x = xx;
		y = yy;
	}
	punto(double xx, double yy){
		id = -1;
		x = xx;
		y = yy;
	}
	punto(const punto &o){
		id = o.id;
		x = o.x;
		y = o.y;
	}
	bool operator==(const punto& o) const {
		return id==o.id && x==o.x && y==o.y;
	}
};

const double EARTH_RADIUS = 6371000.0;
const double EPS = 1e-9;
const double MAX_RADIUS = 500.;

double degToRad(double d){
	d = abs(d);
	return d*M_PI/180.;
}

punto degToRad(punto &p){
	return punto(p.id, degToRad(p.x), degToRad(p.y));
}

double distance1D(double x, double m, double M){
	if(x + EPS > m && x - EPS < M) return 0.0;
	if(x - EPS < m) return m - x;
	return x - M;
}

double distInMeters(punto p, punto q){
	p = degToRad(p);
	q = degToRad(q);
	
	double dlon = q.y - p.y;
	double dlat = q.x - p.x;
	double lat1 = p.x;
	double lat2 = q.x;
	
	double computation = asin(sqrt(sin(dlat / 2) * sin(dlat / 2) + cos(lat1) * cos(lat2) * sin(dlon / 2) * sin(dlon / 2)));
	return 2 * EARTH_RADIUS * computation;
}

punto min(punto &a, punto &b){
	return punto(min(a.x, b.x), min(a.y, b.y));
}

punto max(punto &a, punto &b){
	return punto(max(a.x, b.x), max(a.y, b.y));
}

punto promedio(punto a, punto b){
	return punto((a.x + b.x)/2, (a.y+b.y)/2);
}

struct SegTree2D{
	punto minimo, maximo;
	int cantPuntos;
	bool soyHoja;
	punto valor;
	vector<vector<SegTree2D>> hijos;
	
	SegTree2D(){
		minimo = punto();
		maximo = punto();
		soyHoja = true;
		valor = punto();
	}
	
	SegTree2D(vector<punto> &ps){
		cantPuntos = (int)(ps.size());
		if(cantPuntos == 0){
			minimo = punto();
			maximo = punto();
			soyHoja = true;
			valor = punto();
		} else {
			valor = punto();
			minimo = ps[0];
			maximo = ps[0];
			for(int i=0; i<(int)(ps.size()); i++){
				minimo = min(minimo, ps[i]);
				maximo = max(maximo, ps[i]);
			}
			soyHoja = (maximo == minimo);
			if(soyHoja){
				valor = ps[0];
			} else {
				punto prom = promedio(minimo, maximo);
				vector<vector<vector<punto>>> vHijos(2, vector<vector<punto>>(2));
				for(int i=0; i<(int)(ps.size()); i++){
					pair<int,int> indice = {(ps[i].x > prom.x), (ps[i].y > prom.y)};
					vHijos[indice.first][indice.second].push_back(ps[i]);
				}
				hijos = vector<vector<SegTree2D>>(2, vector<SegTree2D>(2, SegTree2D()));
				for(int i = 0; i < 2; i++){
					for(int j = 0; j < 2; j++){
						SegTree2D h = SegTree2D(vHijos[i][j]);
						hijos[i][j] = h;
					}
				}
			}
		}
	}
	double dist(punto &p){
		double dh = distance1D(p.x, minimo.x, maximo.x);
		double dv = distance1D(p.y, minimo.y, maximo.y);
		return distInMeters(p, punto(p.x + dh, p.y + dv));
	}
	punto puntoMasCercano(punto p){
		if(soyHoja || dist(p) > MAX_RADIUS){
			return valor;
		}
		punto res = valor;
		for(int i=0; i<2; i++){
			for(int j=0; j<2; j++){
				punto nuevo = hijos[i][j].puntoMasCercano(p);
				if(distInMeters(p, nuevo) < distInMeters(p, res)){
					res = nuevo;
				}
			}
		}
		return res;
	}
};

struct linea{
	int id;
	vector<punto> ps;
	SegTree2D st;
};

int main(){
	int N; cin >> N;
	vector<linea> v(N);
	for(int i=0; i<N; i++){
		cin >> v[i].id;
		int nPuntos; cin >> nPuntos;
		v[i].ps = vector<punto>(nPuntos);
		for(int j=0; j<nPuntos; j++){
			v[i].ps[j].id = j;
			cin >> v[i].ps[j].y >> v[i].ps[j].x;
		}
		v[i].st = SegTree2D(v[i].ps);
	}
	
	long long int M; cin >> M;
	int puntosAlcanzables = 0;
	for(int i=0; i<M; i++){
		punto p;
		cin >> p.id >> p.y >> p.x;
		vector<pair<int,punto>> paradasCercanas;
		for(int j=0; j<N; j++){
			punto masCercano = v[j].st.puntoMasCercano(p);
			if(masCercano.id == -1) continue;
			if(distInMeters(masCercano, p) - EPS < MAX_RADIUS){
				paradasCercanas.push_back({v[j].id, masCercano});
			}
		}
		if(paradasCercanas.size() > 0){
			puntosAlcanzables++;
		}
		for(auto pa : paradasCercanas){
			cout << p.id << "|" << pa.first << "|" << pa.second.id << "\n";
		}
		if(i % 500 == 0){
			cerr << i+1 << " processed points. " << puntosAlcanzables << " with bus data.\n";
		}
	}
}
