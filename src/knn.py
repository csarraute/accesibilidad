import sys, os
import json
from util import distCuadrado, cargarColectivos
import datetime

INF = 1000000000000.

def printLog(s):
	print datetime.datetime.now(), s

def printUsage():
	print "Usage: python knn.py [source_points] [dest_points] [k] [mode]"

# Funcion para buscar k vecinos mas cercanos a vuelo de pajaro
def findKNearest(s, d, k):
	resSource = []
	for idx2 in d:
		p2 = d[idx2]
		dNueva = distCuadrado(s['coords'], p2['coords'])
		if len(resSource) < k:
			resSource.append((dNueva, p2))
		elif resSource[k-1][0] > dNueva:
			resSource[k-1] = (dNueva, p2)
		resSource.sort()
	return resSource

def graphFilename(mode):
	return "../data/mapa-" + mode + "/buenos_aires_rectangle.osrm"

def darHorario(d, cs):
	l = int(d['id_linea'])
	return cs[l]['paradas'][int(d['id_parada'])]['horario']

# Devuelve el tiempo en colectivo desde una parada a otra
# Si queremos viajar en sentido opuesto al de la linea, devolvemos infinito
def darViajeEnColectivo(dataIni, dataFin, cs):
	l = int(dataFin['id_linea'])
	if 'horario' not in  cs[l]['paradas'][int(dataFin['id_parada'])].keys():
		return INF
	tiempo = darHorario(dataFin, cs) - darHorario(dataIni, cs)
	if tiempo < 0:
		return INF
	return tiempo

# Funcion para calcular tiempos de un origen a un destino con datos de transporte publico
def getMinDistanceBus(p, q, buses):
	# Calculamos tiempo de a pie desde el origen al destino para usar como default cuando no hay colectivos en comun
	minimo = getMinDistances(p, [q], 'pie', buses)[0]
	# Si no hay colectivos que pasen cerca del origen, devolvemos el tiempo de caminata
	if 'paradas' not in p.keys():
		return minimo
	for l in p['paradas']: # Por cada parada cercana al origen
		# Si el destino no tiene paradas, no podemos hacer nada
		if 'paradas' not in q.keys():
			continue
		# Si esta linea pasa cerca del destino, calculamos el viaje en colectivo
		if l in q['paradas'].keys():
			dataP = p['paradas'][l]
			dataQ = q['paradas'][l]
			viajeEnColectivo = darViajeEnColectivo(dataP, dataQ, buses)
			tiempoNuevo = dataP['tiempo'] + dataQ['tiempo'] + viajeEnColectivo
			minimo = min(minimo, tiempoNuevo)
	return minimo

# Funcion para calcular tiempos de un origen a varios destinos con OSRM o datos de transporte publico
def getMinDistances(p, qs, mode, buses):
	# Si el modo es transporte publico, no usamos OSRM
	if mode == "transporte-publico":
		return [getMinDistanceBus(p, q, buses) for q in qs]
	
	# Si el modo NO es transporte publico, creamos el comando para llamar a OSRM
	tokens = ["./build/osrm-distances", graphFilename(mode)]
	tempFilename = "../data/temp_knn.txt"
	for x in p['coords']: # Agregamos origen
		tokens.append(str(x))
	for q in qs: # Agregamos destinos
		for x in q['coords']:
			tokens.append(str(x))
	tokens.append(">")
	tokens.append(tempFilename)
	os.system((" ").join(tokens)) # Hacemos query a OSRM

	with(open(tempFilename)) as f: # Parseamos resultados
		return [float(x) for x in f.read().strip().split(" ")]
		
def getMinDistance(p, qs, mode, buses):
	dists = getMinDistances(p, qs, mode, buses)
	return min(dists)

# Funcion para agregar tiempo a de caminata desde un punto a las paradas que quedan cerca
def addDistancesToBusStops(ps, cs):
	for e,idx in enumerate(ps):
		if e%1000 == 0:
			printLog(str(e) + " puntos procesados.")
		p = ps[idx]
		if 'paradas' not in p.keys(): continue # Si no tiene paradas, no hacemos nada
		lineaParada = [p['paradas'][k] for k in p['paradas'].keys()]
		coords = [cs[int(lp['id_linea'])]['paradas'][int(lp['id_parada'])] for lp in lineaParada]
		# Calculamos tiempo a paradas caminando con OSRM
		dists = getMinDistances(p, coords, 'pie', cs)
		for e,lp in enumerate(lineaParada):
			p['paradas'][lp['id_linea']]['tiempo'] = dists[e]
		ps[idx] = p
	return ps

def nearestElement(p, candidates, mode, buses):
	return getMinDistance(p, candidates, mode, buses)

def filterName(s):
	l = s.split("/")
	f = l[-1].split(".")
	return f[0]

def getOutFileName(s, d, m):
	sName = filterName(s)
	dName = filterName(d)
	return "../data/out/" + sName + "_to_" + dName + "_by_" + m + ".csv"

def main(argc, argv):
	# Argumentos que se toman
	sourceFile = argv[1] # Puntos origen
	destFile = argv[2] # Puntos destino
	k = int(argv[3]) # k de kNN
	mode = argv[4] # Tipo de transporte (auto, bici, a pie, transporte publico)

	# Cargamos puntos de destino
	dest = []
	with(open(destFile, 'r')) as f:
		dest = json.load(f)
		printLog("Se termino de cargar el archivo destino. " + str(len(dest)) + " puntos.")

	# Cargamos puntos de origen
	src = []
	with(open(sourceFile, 'r')) as f:
		src = json.load(f)
		printLog("Se termino de cargar el archivo fuente. " + str(len(src)) + " puntos.")
	
	buses = {}
	if mode == "transporte-publico":
		# Si usamos transporte publico, cargamos los colectivos
		busesLista = cargarColectivos()
		buses = {}
		for b in busesLista:
			buses[b['id']] = b
		printLog("Se termino de cargar la base de colectivos")
		# Ademas, calculamos el tiempo de caminata del origen y destino a las paradas cercanas
		dest = addDistancesToBusStops(dest, buses)
		printLog("Se termino de agregar los tiempos a los puntos de destino")
		src = addDistancesToBusStops(src, buses)
		printLog("Se termino de agregar los tiempos a los puntos fuente")
		
	with(open(getOutFileName(sourceFile, destFile, mode), "w")) as f:
		for e,idx in enumerate(src):
			if e%1000 == 0:
				printLog(str(e) + " de " + str(len(src)) + " puntos procesados.")
			p = src[idx]
			# Buscamos k vecinos mas cercanos a vuelo de pajaro
			candidates = [x[1] for x in findKNearest(p, dest, k)]
			# Nos quedamos con el mas cercano segun OSRM
			pointToDistance = [p['id'], p['coords'][0], p['coords'][1], nearestElement(p, candidates, mode, buses)]
			f.write(("|").join([str(x) for x in pointToDistance]) + "\n")

if __name__ == '__main__':
	argc = len(sys.argv)
	if(argc < 5):
		printUsage()
	else:
		main(argc, sys.argv)
