#include "osrm/match_parameters.hpp"
#include "osrm/nearest_parameters.hpp"
#include "osrm/route_parameters.hpp"
#include "osrm/table_parameters.hpp"
#include "osrm/trip_parameters.hpp"

#include "osrm/coordinate.hpp"
#include "osrm/engine_config.hpp"
#include "osrm/json_container.hpp"

#include "osrm/osrm.hpp"
#include "osrm/status.hpp"

#include <exception>
#include <iostream>
#include <string>
#include <utility>

#include <cstdlib>
#include <cmath>

#define INF 99999999

int main(int argc, const char *argv[]) {
    if (argc < 2) {
        std::cerr << "Uso: " << argv[0] << " data.osrm src dst..\n";
        return EXIT_FAILURE;
    }

    using namespace osrm;

    // Configure based on a .osrm base path, and no datasets in shared mem from osrm-datastore
    EngineConfig config;

    config.storage_config = {argv[1]};
    config.use_shared_memory = false;

    // We support two routing speed up techniques:
    // - Contraction Hierarchies (CH): requires extract+contract pre-processing
    // - Multi-Level Dijkstra (MLD): requires extract+partition+customize pre-processing
    //
    // config.algorithm = EngineConfig::Algorithm::CH;
    config.algorithm = EngineConfig::Algorithm::MLD;

    const OSRM osrm{config};

    TableParameters params;

    params.sources.push_back(0);
    params.coordinates.push_back({util::FloatLongitude{stof((std::string) argv[2])}, util::FloatLatitude{stof((std::string) argv[3])}});

    for (int i = 4; i < argc; i+=2) {
        params.coordinates.push_back({util::FloatLongitude{stof((std::string) argv[i])}, util::FloatLatitude{stof((std::string) argv[i + 1])}});
        params.destinations.push_back((i-2)/2);
    }


    // Response is in JSON format
    json::Object result;

    // Execute routing request, this does the heavy lifting
    const auto status = osrm.Table(params, result);

    if (status == Status::Ok) {
        auto& table = result.values["durations"].get<json::Array>();
        auto& durations = table.values.at(0).get<json::Array>();

        double bestDuration = INF;//durations.values.at(0).get<json::Number>().value;
        for (auto& t : durations.values) {
            bestDuration = std::min(bestDuration, t.is<json::Null>()? INF: t.get<json::Number>().value);
        }
        std::cout << bestDuration << std::endl;

        return EXIT_SUCCESS;
    } else if (status == Status::Error) {
        const auto code = result.values["code"].get<json::String>().value;
        const auto message = result.values["message"].get<json::String>().value;

        std::cout << "Code: " << code << "\n";
        std::cout << "Message: " << code << "\n";
        return EXIT_FAILURE;
    }
}
