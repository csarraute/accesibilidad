import csv, sys
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from random import random
from util import jsonSave

# Este script genera puntos aleatorios dentro de los poligonos de los barrios de CABA
# Uso:
# python puntos-random-cada.py <puntos-por-barrio> <archivo-salida>

PATH_BARRIOS = "../data/barrios.csv"
PATH_POBLACIONES = "../data/poblacion.csv"

def filtrarCoord(s):
	numbers = "-1234567890."
	return ''.join(c for c in s if c in numbers)


def stringACoord(s):
	return float(filtrarCoord(s))

def procesarBarrio(row, pobs):
	# Busco la poblacion correspondiente
	nombre = row[1]
	pob = pobs[nombre]
	# Me quedo con las coordenadas separando por coma
	coords = row[0].split(',')
	# Saco el POLYGON
	coords[0] = coords[0].split('(')[-1]
	# Paso a lista de long lat
	coords = [(stringACoord(c.split(' ')[0]), stringACoord(c.split(' ')[1])) for c in coords]
	return {'nombre': nombre, 'poblacion': pob, 'poligono': coords}


def cargarBarrios():
	barrios = []
	poblaciones = {}
	# Cargo las poblaciones de los barrios
	with open(PATH_POBLACIONES) as f:
		for neigh in f:
			data = neigh.strip().split('|')
			poblaciones[unicode(data[0].upper())] = int(data[1])
	# Cargo los poligonos de los barrios
	with open(PATH_BARRIOS) as f:
		csvReader = csv.reader(f)
		next(csvReader) # Salteo el header
		barrios = [procesarBarrio(row, poblaciones) for row in csvReader]
	
	return barrios

# Genera un punto random en el rectangulo que resulta
# de los maximos y minimos de latitud y longitud del poligono
def generarPunto(minLong, maxLong, minLat, maxLat):
	dLong = maxLong - minLong
	dLat = maxLat - minLat
	return (minLong + random() * dLong, minLat + random() * dLat)


def generarPuntoEnArea(area, i):
	minLong = min(x[0] for x in area)
	maxLong = max(x[0] for x in area)
	minLat = min(x[1] for x in area)
	maxLat = max(x[1] for x in area)

	poligono = Polygon(area)

	# Genera puntos random en el rectangulo antes dicho (que es relativamente facil y barato computacionalmente)
	# hasta que consiga un punto que pertenezca realmente al poligono
	punto = {"coords": generarPunto(minLong, maxLong, minLat, maxLat), "id": i}
	while not poligono.contains(Point(punto["coords"])):
		punto = {"coords": generarPunto(minLong, maxLong, minLat, maxLat), "id": i}
	
	return punto


def main(argc, argv):
	if argc != 3:
		print "Tenes que pasar un numero de puntos totales (aproximado) y el nombre del archivo de salida"
		print "Ejemplo: python puntos-random-caba.py 2000 out.json"
		return

	barrios = cargarBarrios()
	poblacionTotal = sum([barrio['poblacion'] for barrio in barrios])
	puntosTotales = int(argv[1])
	factor = puntosTotales * 1. / poblacionTotal

	puntos = {}
	# Por cada barrio
	for (e,barrio) in enumerate(barrios):
		# Es +0.5 para que redondee al calcular la cantidad de puntos
		puntosBarrio = int(barrio['poblacion'] * factor + 0.5)
		print "Generando", puntosBarrio, "puntos en", barrio['nombre']
		# Generamos n puntos que esten en dicho poligono
		for i in range(puntosBarrio):
			idx = len(puntos)
			puntos[idx] = generarPuntoEnArea(barrio['poligono'], idx)

	print "Se generaron en total", len(puntos), "puntos."
	jsonSave(argv[2], puntos)


if __name__ == '__main__':
	main(len(sys.argv), sys.argv)
