#include "osrm/match_parameters.hpp"
#include "osrm/nearest_parameters.hpp"
#include "osrm/route_parameters.hpp"
#include "osrm/table_parameters.hpp"
#include "osrm/trip_parameters.hpp"

#include "osrm/coordinate.hpp"
#include "osrm/engine_config.hpp"
#include "osrm/json_container.hpp"

#include "osrm/osrm.hpp"
#include "osrm/status.hpp"

#include <exception>
#include <iostream>
#include <string>
#include <utility>

#include <cstdlib>
#include <cmath>

#define INF 99999999

// Script que calcula la distancia/tiempo de un origen a k destinos
int main(int argc, const char *argv[]) {
    if (argc < 2) {
        std::cerr << "Uso: " << argv[0] << " data.osrm [--distance] src dst..\n";
        return EXIT_FAILURE;
    }

    using namespace osrm;

    // Configure based on a .osrm base path, and no datasets in shared mem from osrm-datastore
    EngineConfig config;

    config.storage_config = {argv[1]};
    config.use_shared_memory = false;

    bool distanceMode = ((std::string) argv[2] == "--distance");

    // We support two routing speed up techniques:
    // - Contraction Hierarchies (CH): requires extract+contract pre-processing
    // - Multi-Level Dijkstra (MLD): requires extract+partition+customize pre-processing
    //
    // config.algorithm = EngineConfig::Algorithm::CH;
    config.algorithm = EngineConfig::Algorithm::MLD;

    const OSRM osrm{config};

    TableParameters params;

    params.sources.push_back(0);
    unsigned int coordsIndex = (distanceMode? 3:2);
    params.coordinates.push_back({
        util::FloatLongitude{stof((std::string) argv[coordsIndex])},
        util::FloatLatitude{stof((std::string) argv[coordsIndex + 1])}
        });

    for (coordsIndex += 2; coordsIndex < argc; coordsIndex += 2) {
        params.coordinates.push_back({
            util::FloatLongitude{stof((std::string) argv[coordsIndex])},
            util::FloatLatitude{stof((std::string) argv[coordsIndex + 1])
            }});
        params.destinations.push_back((coordsIndex-2)/2);
    }

    if (distanceMode) {
        params.annotations = engine::api::TableParameters::AnnotationsType::Distance;
    }


    // Response is in JSON format
    json::Object result;

    // Execute routing request, this does the heavy lifting
    const auto status = osrm.Table(params, result);

    if (status == Status::Ok) {
        json::Array table;
        if (distanceMode) {
            table = result.values["distances"].get<json::Array>();
        } else {
            table = result.values["durations"].get<json::Array>();
        }

        // Solo tengo un origen
        auto& results = table.values.at(0).get<json::Array>();

        for (auto& t : results.values) {
            double result = t.is<json::Null>()? INF: t.get<json::Number>().value;
            std::cout << result << " ";
        }

        std::cout << std::endl;

        return EXIT_SUCCESS;
    } else if (status == Status::Error) {
        const auto code = result.values["code"].get<json::String>().value;
        const auto message = result.values["message"].get<json::String>().value;

        std::cout << "Code: " << code << "\n";
        std::cout << "Message: " << code << "\n";
        return EXIT_FAILURE;
    }
}
