import json, sys, os

def distCuadrado(p1, p2):
	d1 = p2[1] - p1[1]
	d2 = p2[0] - p1[0]
	return d1**2 + d2**2


def cargarColectivos():
	colectivos = []
	with(open("../data/transporte-publico/recorrido-colectivos-horarios.json")) as f:
		colectivos = json.load(f)

	return colectivos


def printAndRun(command):
	print("Running", command)
	return os.system(command)


def jsonSave(filename, data):
	with(open(filename, "w")) as f:
		json.dump(data, f)
