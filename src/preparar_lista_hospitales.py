import sys, os
import json
from unidecode import unidecode

def normalize(s):
	return unidecode(s.decode('utf-8'))

def cargarListaHospitales():
	f_hospitales = "../data/centros de salud/base_de_centros_de_salud_combinada.geojson"
	with(open(f_hospitales)) as f:
		hospitales = json.load(f)['features']
		
	f_categorias = "../data/centros de salud/tabla_de_categorias.csv"
	with(open(f_categorias)) as f:
		categorias = [line.split(",") for line in f.read().strip().split('\n')[1:]]
	
	mapa_categorias = dict()
	for l in categorias:
		# Quien manda a Tierra del Fuego a tener una coma en el nombre...
		if(len(l) > 4):
			l = [l[0], (','.join(l[1:-2]))[1:-1], l[-2], l[-1]]
		
		clase = normalize(l[0])
		prov = normalize(l[1])
		cat = normalize(l[3])
		
		if clase not in mapa_categorias.keys():
			mapa_categorias[clase] = dict()
		mapa_categorias[clase][prov] = cat
	
	hospitales_limpio = []
	for h in hospitales:
		if h['properties']['clase'] is None: continue
		clase = unidecode(h['properties']['clase'])
		prov = unidecode(h['properties']['prov'])
		
		if mapa_categorias[clase][prov] in ["Hospital", "Centro de Salud"]:
			coords = h['geometry']['coordinates']
			hospitales_limpio.append(coords)

	return hospitales_limpio

def main(argc, argv):
	hospitales = cargarListaHospitales()
	hospitalesJSON = {}
	for (e,h) in enumerate(hospitales):
		hospitalesJSON[e+1] = {"id": e+1, "coords": h}
	with(open(argv[1], "w")) as f:
		json.dump(hospitalesJSON, f)

if __name__ == '__main__':
	main(len(sys.argv), sys.argv)
