import numpy as np
import matplotlib.pyplot as plt
import sys, os
import json

def printUsage():
	print "Usage:"
	print "python histograms.py [path] [bins]"

def outfile(p):
	tokens = p.split("/")
	name = "../data/plots/" + (tokens[-1].split(".")[0]).replace("_", "-")
	print "Saving file in", name + ".png"
	return name

def printHist(a, bin_limit, path, t=""):
	plt.hist(a, bins=np.arange(bin_limit, step=60))
	plt.xlabel("seconds")
	plt.title(t)
	plt.savefig(path)

def main(argc, argv):
	if argc < 2:
		printUsage()
		return 1
	
	MIPATH = argv[1]
	bin_limit = 1000
	if(argc > 2):
		bin_limit = int(argv[2])
	
	data = {}
	with(open(MIPATH)) as f:
		data = [x.split("|") for x in f.read().strip().split("\n")]
	
	serie = [float(d[3]) for d in data]
	printHist(serie, bin_limit, outfile(MIPATH))

if __name__ == '__main__':
	main(len(sys.argv), sys.argv)
