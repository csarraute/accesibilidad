import sys, os
import csv
import json
from util import cargarColectivos, printAndRun

# Este script agrega los colectivos alcanzables a un conjunto de puntos
# Requiere haber compilado previamente min_distances.cpp
# El uso es
# python colectivos-alcanzables.py <points_file>

def procesarParadas(l):
    ps = {}
    for p in l:
        tokens = p.split("|")
        ps[int(tokens[0])] = int(tokens[1])	# (k, v) = (id_linea, id_parada)
    return ps

def main(argc, argv):
    pointsFile = argv[1]
    
    tempFileName = "../data/transporte-publico/temp-colectivos-alcanzables.txt"
    # Cargamos los colectivos desde data/transporte-publico/recorrido-colectivos-horarios.json
    colectivos = cargarColectivos()
    # Guardamos temporalmente los datos de los colectivos en un archivo de forma que sea mas facil
    # tener estos datos al llamar a min_distances.cpp
    with(open(tempFileName, "w")) as f:
        f.write(str(len(colectivos)) + "\n")
        for c in colectivos:
            f.write(str(c["id"]) + " " + str(len(c["paradas"])) + "\n")
            for p in c["paradas"]:
                f.write(" " + str(p["coords"][0]) + " " + str(p["coords"][1]))
            f.write("\n")

    # Cargamos los puntos a procesar
    points = []
    with(open(pointsFile, "r")) as f:
		points = json.load(f)

    # Agregamos al archivo temporal los puntos a procesar
    with(open(tempFileName, "a")) as fTemp:
        fTemp.write(str(len(points)) + "\n")
        for p in points:
            e = points[p]
            s = str(e["id"]) + " " + (" ").join([str(n) for n in e["coords"]])
            fTemp.write(s + "\n")

    # Llamamos a min_distances.cpp para que haga el trabajo mas eficientemente
    outFileName = "../data/transporte-publico/temp-colectivos-alcanzables.out"
    bash_command = "./min_distances < " + tempFileName + " > " + outFileName
    printAndRun(bash_command)
    # Limpiamos el archivo temporal
    bash_command = "rm -rf " + tempFileName
    printAndRun(bash_command)
    
    # Parseamos el resultado
    with(open(outFileName)) as f:
        for line in f:
            tokens = line.strip().split("|")
            if "paradas" not in points[tokens[0]].keys():
                points[tokens[0]]["paradas"] = {}
                
            points[tokens[0]]["paradas"][tokens[1]] = {"id_linea": tokens[1], "id_parada": tokens[2]}

    bash_command = "rm -rf " + outFileName
    printAndRun(bash_command)
    
    with(open(pointsFile, "w")) as f:
        json.dump(points, f)

if __name__ == '__main__':
    main(len(sys.argv), sys.argv)
