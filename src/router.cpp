#include "osrm/match_parameters.hpp"
#include "osrm/nearest_parameters.hpp"
#include "osrm/route_parameters.hpp"
#include "osrm/table_parameters.hpp"
#include "osrm/trip_parameters.hpp"

#include "osrm/coordinate.hpp"
#include "osrm/engine_config.hpp"
#include "osrm/json_container.hpp"

#include "osrm/osrm.hpp"
#include "osrm/status.hpp"

#include <exception>
#include <iostream>
#include <string>
#include <utility>

#include <cstdlib>

// Script que calcula la distancia/tiempo entre dos puntos consecutivos de un camino
int main(int argc, const char *argv[])
{
    if (argc < 2)
    {
        std::cerr << "Usage: " << argv[0] << " data.osrm [--distance] coords..\n";
        return EXIT_FAILURE;
    }

    using namespace osrm;

    // Configure based on a .osrm base path, and no datasets in shared mem from osrm-datastore
    EngineConfig config;

    config.storage_config = {argv[1]};
    config.use_shared_memory = false;

    bool distanceMode = ((std::string) argv[2] == "--distance");

    // We support two routing speed up techniques:
    // - Contraction Hierarchies (CH): requires extract+contract pre-processing
    // - Multi-Level Dijkstra (MLD): requires extract+partition+customize pre-processing
    //
    // config.algorithm = EngineConfig::Algorithm::CH;
    config.algorithm = EngineConfig::Algorithm::MLD;

    // Routing machine with several services (such as Route, Table, Nearest, Trip, Match)
    const OSRM osrm{config};

    RouteParameters params;

    for (int coordsIndex = (distanceMode? 3:2); coordsIndex < argc; coordsIndex+=2) {
        params.coordinates.push_back({
            util::FloatLongitude{std::stof((std::string) argv[coordsIndex])},
            util::FloatLatitude{std::stof((std::string) argv[coordsIndex+1])}
            });
    }

    // Response is in JSON format
    json::Object result;

    // Execute routing request, this does the heavy lifting
    const auto status = osrm.Route(params, result);

    if (status == Status::Ok)
    {
        auto &routes = result.values["routes"].get<json::Array>();

        // Let's just use the first route
        auto &route = routes.values.at(0).get<json::Object>();
        // const auto distance = route.values["distance"].get<json::Number>().value;
        // const auto duration = route.values["duration"].get<json::Number>().value;

        // Warn users if extract does not contain the default coordinates from above
        // if (distance == 0 || duration == 0)
        // {
        //     std::cout << "Note: distance or duration is zero. ";
        //     std::cout << "You are probably doing a query outside of the OSM extract.\n\n";
        // }

        // std::cout << "Distance: " << distance << " meter\n";
        // std::cout << "Duration: " << duration << " seconds\n";

        const auto legs = route.values["legs"].get<json::Array>();
        std::string key = (distanceMode?"distance":"duration");
        for (const auto l :legs.values) {
            const auto d = l.get<json::Object>().values.at(key).get<json::Number>().value;
            std::cout << d << " ";
        }

        std::cout << std::endl;

        return EXIT_SUCCESS;
    } else if (status == Status::Error){
        const auto code = result.values["code"].get<json::String>().value;
        const auto message = result.values["message"].get<json::String>().value;

        std::cout << code << "\n";
        return EXIT_FAILURE;
    }
}
