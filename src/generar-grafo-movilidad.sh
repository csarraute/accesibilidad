#!/bin/bash

ESPANIOL=(auto pie bici)
INGLES=(car foot bicycle)

cd ../data
for index in ${!ESPANIOL[*]}
do
	mkdir mapa-${ESPANIOL[$index]}
	cd mapa-${ESPANIOL[$index]}
	cp ../buenos_aires_rectangle.osm .
	osrm-extract buenos_aires_rectangle.osm -p ~/osrm-backend/profiles/${INGLES[$index]}.lua
	osrm-partition buenos_aires_rectangle.osrm
	osrm-customize buenos_aires_rectangle.osrm
	cd ..
done

