import sys, os
import json
import csv
from util import jsonSave

# Este script se encarga de procesar los datos de CAF para dejarlos en un formato
# que podamos usar en el resto de scripts
# Este formato esta explicado en el tech-report

# Elimina caracteres que no van, pone punto donde corresponde y pasa a float
# Asume que el punto va en la posicion 3 ya que las coords de Bs As son -34 y -58
def parseCoord(s):
	numbers = "-1234567890"
	filtered = ''.join(c for c in s if c in numbers)
	return float('.'.join([filtered[:3], filtered[3:]]))


# Devuelve un mapa id -> {id, coords}
def loadData(filename, processor):
	data = {}
	# Cargamos los datos y los pasamos al formato
	with open(filename) as f:
		csvReader = csv.reader(f)
		next(csvReader) # Salteo el header
		for row in csvReader:
			place = processor(row)
			data[place['id']] = place

	return data

	

def main(argc, argv):
	# Datos de archivos a cargar
	# Los lambdas estan ya que los distintos archivos tienen los campos en distinto orden
	filesPrefix = "../data/caf/"
	files = [
		{
			'filename': 'CAF_BANK',
			'processor': lambda r: {
				'id': int(r[1]),
				'coords': [parseCoord(r[4]), parseCoord(r[3])]
				}
		},
		{
			'filename': 'CAF_ESC',
			'processor': lambda r: {
				'id': int(r[0]),
				'coords': [parseCoord(r[2]), parseCoord(r[3])]
				}
		},
		{
			'filename': 'CAF_FFAA',
			'processor': lambda r: {
				'id': int(r[0]),
				'coords': [parseCoord(r[3]), parseCoord(r[2])]
				}
		},
		{
			'filename': 'CAF_Viviendas',
			'processor': lambda r: {
				'id': int(r[0]),
				'coords': [parseCoord(r[2]), parseCoord(r[1])]
				}
		},
	]

	for file in files:
		data = loadData(filesPrefix + file['filename'] + '.csv', file['processor'])
		jsonSave(filesPrefix + file['filename'] + '.json', data)


if __name__ == '__main__':
	main(len(sys.argv), sys.argv)
