# Fuentes de datos

Dataset de colectivos: https://data.buenosaires.gob.ar/dataset/colectivos
Para trabajar, se pasó el archivo recorrido-colectivos.csv a UTF-8:
```
iconv -f ISO-8859-14 -t UTF-8 recorrido-colectivos.csv -o recorrido-colectivos-utf8.csv
```

Preprocesamos para generar:
id-linea, nombre-linea, lat, lon, tiempo
Donde nombre-linea es "numero - ramal - sentido" y tiempo es el tiempo acumulado
desde la terminal hasta esta parada.

El preprocesamiento se realizó desde el directorio src, ejecutando:
```
python armar-json-de-paradas.py
python preparar-paradas-colectivos-route.py
```
donde el primer script pasa el csv a json y el segundo agrega los horarios de paradas.
