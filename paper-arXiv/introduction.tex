%!TEX root = paper-eng.tex

%--------------------------------------------------------------------------
\section{Introduction}
%--------------------------------------------------------------------------


%--------------------------------------------------------------------------
\subsection{Problem and context}
%--------------------------------------------------------------------------

Urban sprawl, socioeconomic segregation, and the impacts of investments in infrastructure and transportation networks are issues whose common factor is to affect the dynamics of cities and ultimately the wellbeing of people that reside in urban areas, which in the developing world is the vast majority. These problems are influenced by geographic accessibility, that is, the possibility that all people can easily transit, through the use of existing infrastructure, to destinations of their own interest. In the last decade, there has been a focus on generating measures capable of determining the quality of accessibility to destinations such as health centers, firms and schools, because  all of them play a major role in determining many economic and social outcomes of households.

When estimating accessibility, it is common to use online services such as Google Maps. However, these application servers have multiple disadvantages: they function as black boxes and do not allow users to modify their content (e.g., adding information on streets or addresses), they have limitations when searching for large amounts of data and they depend on internet connection to perform any kind of query. In addition, they have information retention policies, which --in many cases-- may conflict with user confidentiality and with the security of the data used in the search.

We propose the creation of the first open tool capable of providing information about atomistic geographic accessibility in Argentina, completely transparent, free, offline and adaptable to the user's needs. This tool contemplates public transport networks and is flexible to the addition of many sets of destinations (``opportunities'') of interest. In addition, it operates with specific locations (that is, both origins and destinations should include geographic coordinates), so it will function as a building block when constructing indicators at lower granularity levels, such as municipalities, provinces or states. 

With this tool, the user will be able to specify the mode used to calculate accessibility: on foot, by bicycle, by car or by public transport. The program will yield a set of measures (distance and time) which will serve as raw material for the preparation of subsequent more elaborate accessibility metrics. Possible uses of this tool are:

\begin{itemize}
\item the construction of indexes capable of summing up in only one dimension the concept of accessibility from a given set of addresses (homes) to the most relevant (economic and social) opportunities for household members,
\item comparisons of accessibility metrics in one specific domain (e.g., distance to a subset of firms or establishments, for instance to those where average paid wages are above a certain threshold) across different origins, which can take the form of isochrones or other ways to summarize the spatial dispersion in the domain of interest,
\item visualizations of saturation indicators (e.g., heat maps) that properly transmit information about opportunities that are accessible or reachable to households residing in different neighborhoods of a given city, as a way to convey useful information for residential and housing decisions.
\end{itemize}


In all these examples, it is not only necessary to have a tool capable of measuring urban accessibility, but also to have detailed information of both starting points (homes) and arrivals (opportunities). In this aspect, the Ministry of Production and Labor (MPyT) has an outstanding record in the field, handling administrative records as well as surveys and censuses, which allow us to characterize households and to know their potential needs with respect to opportunities. Likewise, in the use cases discussed in Section~\ref{use-cases}, we will make use of some open databases (e.g., schools, hospitals, banks, etc.) that are available nationwide and which contain georeferenced and some other useful descriptive features of these type of destinations.


In addition to the new lines of research opened by this tool, we can list other areas where its application may add a quantitative dimension to the already existing approach --usually qualitative, or quantitative but lacking the spatial dimension behind the policy issue-- for designing and analyzing relevant public policies. For instance, this tool can help to improve the policy discussion around several relevant policy issues: 
	
\begin{itemize}
\item The analysis of spatial labor mismatches (analysis of labor supply and demand) as well as the assessment of how existing metrics of accessibility correlate with the degree of misallocation of resources induced by such mismatches.
\item The analysis of commuting time costs, which is usually done by means of (sometimes quite costly) origin-destination surveys which serve as the basis for the design, implementation and evaluation of policies regarding urban transport planning.
\item The analysis of place-based policies, which target places --instead of individuals or firms-- and try to boost economic activity in relatively narrowly defined geographic areas (e.g., enterprise zones).
\item The study of individuals' and families' residential and housing decisions (human mobility and migrations).
\end{itemize}
	


%--------------------------------------------------------------------------
\subsection{Proposed data science product}
%--------------------------------------------------------------------------

The objective of the project is to develop a tool to calculate distances and travel times between:
\begin {itemize}
\item a set of origins,
\item a set of destinations,
\item using different modes of transport.
\end {itemize}

The input data (inputs) for the tool are:
\begin {itemize}
\item Destinations: A geo-referenced list of points of interest or opportunities (such as employers' establishments, schools, banks, libraries, etc.)
For example: a list with over 50,000 schools in Argentina.

\item Origins: The tool includes several ways to specify the origins:
\begin {itemize}
\item List of points (geographic coordinates).
\item For example: coordinates of residences of households surveyed in official large-scale household surveys.
\item All the corners in a given urban area (e.g., the Autonomous City of Buenos Aires, CABA, or the Metropolitan Area of Buenos Aires, also known as AMBA).
\item Centroids of census tracts.
\item Centroids of polygons defining legal limits for municipalities.
\end{itemize}

\end{itemize}

Based on the input data, the tool is responsible for determining, from each origin, which is the closest destination, depending on the distance or travel time and the mode of transport. The options that we contemplate are:
\begin {itemize}
\item Minimum distance:
\begin {itemize}
\item On foot
\item By car

\end {itemize}


\item Minimum travel time:
\begin {itemize}
\item On foot
\item By bicycle
\item By car
\item By public transport.
\end{itemize}


\end{itemize}

It is worth mentioning that the sets of origins and destinations are large sets, since each one can contain up to several thousand points. This makes calculating the minimum path from all origins to all destinations a task that is computationally very inefficient. To overcome this problem, we developed a considerably more efficient implementation that calculates minimum paths.

A second objective of this project was to rely only on open data, that is, to use freely available and freely accessible data, such as Open Street Map (OSM) data. We consider that using open data, together with making this an open source tool, are crucial aspects to not depend on external factors such as the data of a private company or the payed access to an API of a private company. 



%--------------------------------------------------------------------------
\subsection{Related work}
%--------------------------------------------------------------------------

Previous works have recognized not only the importance of using quantitative measures of accessibility in the design of urban policies, but have also shown the inherent complexity of constructing such metrics, specially in the context of developing countries~\cite{red2017}. However, and for the specific case of the city of Buenos Aires, some of the collaborators on this project developed interesting metrics that partially capture the idea of accessibility and that made use of a completely different type of data (cell phone data)~\cite{Anapolsky2014exploracion,Sarraute2017city}. It is worth mentioning that both the approach taken here and the approach in~\cite{Anapolsky2014exploracion,Sarraute2017city} are perfectible compatible and are likely to enrich one another. Some other related work can be found in 
studies on the prediction of human mobility, for instance those that include the analysis of particularly large sociocultural events~\cite{Ponieman2013human,Ponieman2015mobility}, or the analysis of more regular human mobility patterns~\cite{Mucelli2016regularity}, and even the  measurement-driven mobile data traffic modeling~\cite{Oliveira2015measurement}.



